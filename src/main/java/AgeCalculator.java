//import java.util.Scanner;
//import java.time.*;
//
//public class MountainGoat2 {
//
//    public static void main(String[] args) {
//
//
//        System.out.println("Enter username:");
//        Scanner username = new Scanner(System.in); //object formation forScanner
//        String name = username.nextLine();
//        System.out.println("Enter userage:");
//        LocalDate rightnow = LocalDate.now(); //object formation of LocalDate
//        String c = rightnow.toString(); //breaking object into array
//        String[] currentyearString = c.split("-"); //spliting string into array
//        int currentyeaInt = Integer.parseInt(currentyearString[0]); // Parse YearString into YearInteger
//
//        Scanner userage = new Scanner(System.in);
//        int age = userage.nextInt(); // Assigned Input
//
//        if(age > 0 && age < currentyeaInt){
//            int borndate = currentyeaInt - age; //subtracting current year by given age
//            System.out.println("I was born in " + borndate);
//        }else{
//            System.out.println("please enter correct age value ");
//        }
//
//
//    }
//}

import java.time.*;
import java.util.Scanner;

public class AgeCalculator {

    public static void main(String[] args){
        System.out.println("Enter your Date of Birth");
        Scanner userInput = new Scanner(System.in);
        int userDOB = userInput.nextInt();

        int currentYear = Year.now().getValue();

        // Loop until USER entered invalid DOB
        while(userDOB > currentYear || currentYear-userDOB > 100) {
            if(userDOB > currentYear){   // User entered more than currentYear(2019).
                System.out.println("Your DOB can't be " + userDOB + ". Re-enter valid DOB.");
            } else if (userDOB < 0) {    // User enter negative value.
                System.out.println("Your cannot enter negative value. Please enter valid DOB.");
            } else {                     // If user is more than 100 years old.
                System.out.println("You can't be " + (currentYear-userDOB) + " years old. Please enter valid DOB.");
            }
            userInput = new Scanner(System.in);
            userDOB = userInput.nextInt();
        }

        int userAge = currentYear - userDOB;
        System.out.println("...You are " + userAge + " years old...");
    }
}
